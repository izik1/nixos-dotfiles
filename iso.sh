#!/bin/sh

flake_path="$(dirname "$(realpath "$0")")"

nix build "${flake_path}#installMedia.mini.config.system.build.isoImage"
