# Skyler's (nixos) Dotfiles

## 3rd-party

These dotfiles are heavily structurally inspired by https://github.com/wiltaylor/dotfiles,
which is licensed under the MIT license.
See the LICENSE file in the source repo, as well as in this repo at [LICENSES/3rd-party/wiltaylor/dotfiles/LICENSE](LICENSES/3rd-party/wiltaylor/dotfiles/LICENSE).