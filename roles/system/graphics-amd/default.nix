{ pkgs, ... }:
{
  services.xserver.videoDrivers = [ "amdgpu" ];

  environment.systemPackages = with pkgs; [ amdvlk ];
}
