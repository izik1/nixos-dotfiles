{ pkgs, ... }:
{
  services.xserver.videoDrivers = [ "nvidia" ];

  environment.systemPackages = with pkgs; [ vulkan-tools ];
}
