{ lib, pkgs, ... }:
{
  # nixpkgs.config.allowUnfree = true;

  programs.steam = {
    enable = true;
    extraCompatPackages = [ pkgs.proton-ge-bin ];
    gamescopeSession.enable = true;
  };

  programs.gamescope = {
    enable = true;
    capSysNice = true;
  };

  # powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";

  services.udev.packages = [
    pkgs.dolphin-emu
  ];

  environment.systemPackages = [
    pkgs.dolphin-emu
    # pkgs.my.synergism
  ];
}
