
## partially adapted from <https://github.com/nushell/nu_scripts/blob/f9b9636003b80f36ac31b0220d61a41164e42216/custom-completions/cargo/cargo-completions.nu>
# partially written manually

module cargo-completions {
    def "nu-complete cargo packages" [] {
      let metadata = (^cargo metadata --format-version=1 --offline --no-deps)
      if $metadata == '' {
        []
      } else {
        $metadata | from json | get workspace_members | each { url parse } | each { $"($in.path | path parse | get stem)@($in.fragment)" }
      }
    }

    def "nu-complete cargo package-targets" [kind: string] {
      let metadata = (^cargo metadata --format-version=1 --offline --no-deps)
      if $metadata == '' {
        return []
      } else {
        let members = $metadata | from json | get workspace_members
        $metadata | from json | get packages | where id in $members | get targets | flatten | filter { $kind in $in.kind } | get name
      }
    }

    def "nu-complete cargo target-bins" [] {
        nu-complete cargo package-targets "bin"
    }

    def "nu-complete cargo target-tests" [] {
        nu-complete cargo package-targets "tests"
    }

    def "nu-complete cargo target-examples" [] {
        nu-complete cargo package-targets "examples"
    }

    def "nu-complete cargo target-benches" [] {
        nu-complete cargo package-targets "benches"
    }

    def "nu-complete cargo-color-option" [] {
        [
            {value: "auto", description: "Automatically detect if color support is available on the terminal"},
            {value: "always", description: "Always display colors"},
            {value: "never", description: "Never display colors"}
        ]
    }

    def "nu-complete cargo message-format-option" [] {
        [
            {value: "human", description: "(default): Display in a human-readable text format. Conflicts with short and json"},
            {value: "short", description: "Emit shorter, human-readable text messages. Conflicts with human and json"},
            {value: "json", description: "Emit JSON messages to stdout. See the reference for more details. Conflicts with human and short"},
            {value: "json-diagnostic-short", description: "Ensure the rendered field of JSON messages contains the “short” rendering from rustc. Cannot be used with human or short"},
            {value: "json-diagnostic-rendered-ansi", description: "Ensure the rendered field of JSON messages contains embedded ANSI color codes for respecting rustc’s default color scheme. Cannot be used with human or short"},
            {value: "json-render-diagnostics", description: "Instruct Cargo to not include rustc diagnostics in JSON messages printed, but instead Cargo itself should render the JSON diagnostics coming from rustc. Cargo’s own JSON diagnostics and others coming from rustc are still emitted. Cannot be used with human or short"},
        ]
    }

    def "nu-complete cargo-z-flags" [] {
        try { 
            ^cargo -Z help | lines | str trim | filter { || $in starts-with "-Z" } | parse -r '-Z (?<value>[^\s]+\s+)\s+?-- (?<description>.+)'
        }
    }

    def "nu-complete cargo-profiles" [] {
        ["debug", "test", "release", "bench"]
    }

    def "nu-complete cargo-targets" [] {
        # in order of priority: 

        # the most likely to actually work (if it exists it *will* work)
        try {
            return (^rustc --print target-list | lines)
        }

        # `cargo rustc --print target-list` is the nicest thing to try... or at least, it will be once it's stable, if it stabilizes.
        try {
            return (^cargo rustc --print target-list | lines)
        }

        # This only works if the manifest isn't virtual, IE, we aren't in a workspace.
        try {
            ^cargo rustc -- --print target-list | lines
        }
    }
    
    # timings has `=html, json` now but it's also not stable. 
    
    # Compile a local package and all of its dependencies
    export extern "cargo build" [
        --future-incompat-report                                               # Outputs a future incompatibility report at the end of the build
        --message-format: string@"nu-complete cargo message-format-option"     # Error format
        --verbose(-v)                                                          # Use verbose output (-vv very verbose/build.rs output)
        --quiet(-q)                                                            # Do not print cargo log messages
        --color: string@"nu-complete cargo-color-option"                       # Control when colored output is used
        --config: string                                                       # Override a configuration value
        -Z: string@"nu-complete cargo-z-flags"                                 # Unstable (nightly-only) flags to Cargo
        --help(-h)                                                             # Print help
        --package(-p): string@"nu-complete cargo packages"                     # Package(s) to build
        --workspace                                                            # Build all packages in the workspace
        --exclude: string@"nu-complete cargo packages"                         # Exclude packages from the build
        --all                                                                  # Alias for --workspace (deprecated)
        --lib                                                                  # Build only this package's library
        --bins                                                                 # Build all binaries
        --bin: string@"nu-complete cargo target-bins"                          # Build only the specified binary
        --examples                                                             # Build all examples
        --example: string@"nu-complete cargo target-examples"                  # Build only the specified example
        --tests                                                                # Build all tests
        --test: string@"nu-complete cargo target-tests"                        # Build only the specified test target
        --benches                                                              # Build all benches
        --bench: string@"nu-complete cargo target-benches"                     # Build only the specified bench target
        --all-targets                                                          # Build all targets
        --features(-F): string                                                 # Space or comma separated list of features to activate
        --all-features                                                         # Activate all available features
        --no-default-features                                                  # Do not activate the `default` feature
        --release(-r)                                                          # Build artifacts in release mode, with optimizations
        --profile: string@"nu-complete cargo-profiles"                         # Build artifacts with the specified profile
        --jobs(-j): int                                                        # Number of parallel jobs, defaults to # of CPUs
        --keep-going                                                           # Do not abort the build as soon as there is an error (unstable)
        --target: string@"nu-complete cargo-targets"                           # Build for the target triple
        --target-dir: directory                                                # Directory for all generated artifacts
        --artifact-dir: directory                                              # Copy final artifacts to this directory (unstable)
        --build-plan                                                           # Output build graph in JSON (unstable)
        --unit-graph                                                           # Output build graph in JSON (unstable)
        --timings                                                              # Emit build timings
        --manifest-path: path                                                  # Path to Cargo.toml
        --lockfile-path: path                                                  # Path to Cargo.lock (unstable)
        --ignore-rust-version                                                  # Ignore `rust-version` specification in packages
        --locked                                                               # Assert that `Cargo.lock` will remain unchanged
        --offline                                                              # Run without accessing the network
        --frozen                                                               # Equivalent to specifying both --locked and --offline
    ]

    # Check a local package and all of its dependencies for errors
    export extern "cargo check" [
        --future-incompat-report                                               # Outputs a future incompatibility report at the end of the build
        --message-format: string@"nu-complete cargo message-format-option"     # Error format
        --verbose(-v)                                                          # Use verbose output (-vv very verbose/build.rs output)
        --quiet(-q)                                                            # Do not print cargo log messages
        --color: string@"nu-complete cargo-color-option"                       # Control when colored output is used
        --config: string                                                       # Override a configuration value
        -Z: string@"nu-complete cargo-z-flags"                                 # Unstable (nightly-only) flags to Cargo
        --help(-h)                                                             # Print help
        --package(-p): string@"nu-complete cargo packages"                     # Package(s) to check
        --workspace                                                            # Check all packages in the workspace
        --exclude: string@"nu-complete cargo packages"                         # Exclude packages from the check
        --all                                                                  # Alias for --workspace (deprecated)
        --lib                                                                  # Check only this package's library
        --bins                                                                 # Check all binaries
        --bin: string@"nu-complete cargo target-bins"                          # Check only the specified binary(s)
        --examples                                                             # Check all examples
        --example: string@"nu-complete cargo target-examples"                  # Check only the specified example
        --tests                                                                # Check all tests
        --test: string@"nu-complete cargo target-tests"                        # Check only the specified test target
        --benches                                                              # Check all benches
        --bench: string@"nu-complete cargo target-benches"                     # Check only the specified bench target
        --all-targets                                                          # Check all targets
        --features(-F): string                                                 # Space or comma separated list of features to activate
        --all-features                                                         # Activate all available features
        --no-default-features                                                  # Do not activate the `default` feature
        --jobs(-j): int                                                        # Number of parallel jobs, defaults to # of CPUs
        --keep-going                                                           # Do not abort the build as soon as there is an error (unstable)
        --release(-r)                                                          # Check artifacts in release mode, with optimizations
        --profile: string@"nu-complete cargo-profiles"                         # Check artifacts with the specified profile
        --target: string@"nu-complete cargo-targets"                           # Check for the target triple
        --target-dir: directory                                                # Directory for all generated artifacts
        --unit-graph                                                           # Output build graph in JSON (unstable)
        --timings                                                              # Emit build timings
        --manifest-path: path                                                  # Path to Cargo.toml
        --lockfile-path: path                                                  # Path to Cargo.lock (unstable)
        --ignore-rust-version                                                  # Ignore `rust-version` specification in packages
        --locked                                                               # Assert that `Cargo.lock` will remain unchanged
        --offline                                                              # Run without accessing the network
        --frozen                                                               # Equivalent to specifying both --locked and --offline
    ]

    # Run a binary or example of the local package
    export extern "cargo run" [
        --message-format: string@"nu-complete cargo message-format-option"     # Error format
        --verbose(-v)                                                          # Use verbose output (-vv very verbose/build.rs output)
        --quiet(-q)                                                            # Do not print cargo log messages
        --color: string@"nu-complete cargo-color-option"                       # Control when colored output is used
        --config: string                                                       # Override a configuration value
        -Z: string@"nu-complete cargo-z-flags"                                 # Unstable (nightly-only) flags to Cargo
        --help(-h)                                                             # Print help
        --package(-p): string@"nu-complete cargo packages"                     # Package(s) to build
        --bin: string@"nu-complete cargo target-bins"                          # Build only the specified binary
        --example: string@"nu-complete cargo target-examples"                  # Build only the specified example
        --features(-F): string                                                 # Space or comma separated list of features to activate
        --all-features                                                         # Activate all available features
        --no-default-features                                                  # Do not activate the `default` feature
        --jobs(-j): int                                                        # Number of parallel jobs, defaults to # of CPUs
        --keep-going                                                           # Do not abort the build as soon as there is an error (unstable)
        --release(-r)                                                          # Build artifacts in release mode, with optimizations
        --profile: string@"nu-complete cargo-profiles"                         # Build artifacts with the specified profile
        --target: string@"nu-complete cargo-targets"                           # Build for the target triple
        --target-dir: directory                                                # Directory for all generated artifacts
        --unit-graph                                                           # Output build graph in JSON (unstable)
        --timings                                                              # Emit build timings
        --manifest-path: path                                                  # Path to Cargo.toml
        --lockfile-path: path                                                  # Path to Cargo.lock (unstable)
        --ignore-rust-version                                                  # Ignore `rust-version` specification in packages
        --locked                                                               # Assert that `Cargo.lock` will remain unchanged
        --offline                                                              # Run without accessing the network
        --frozen                                                               # Equivalent to specifying both --locked and --offline
    ]

    # Check a local package and all of its dependencies for errors
    export extern "cargo test" [
        --no-run                                                               # Compile, but don't run tests
        --no-fail-fast                                                         # Run all tests regardless of failure
        --future-incompat-report                                               # Outputs a future incompatibility report at the end of the build    
        --message-format: string@"nu-complete cargo message-format-option"     # Error format
        --quiet(-q)                                                            # Do not print cargo log messages
        --verbose(-v)                                                          # Use verbose output (-vv very verbose/build.rs output)
        --color: string@"nu-complete cargo-color-option"                       # Control when colored output is used
        --config: string                                                       # Override a configuration value
        -Z: string@"nu-complete cargo-z-flags"                                 # Unstable (nightly-only) flags to Cargo
        --help(-h)                                                             # Print help
        --package(-p): string@"nu-complete cargo packages"                     # Package(s) to check
        --workspace                                                            # Check all packages in the workspace
        --exclude: string@"nu-complete cargo packages"                         # Exclude packages from the check
        --all                                                                  # Alias for --workspace (deprecated)
        --lib                                                                  # Check only this package's library
        --bins                                                                 # Check all binaries
        --bin: string@"nu-complete cargo target-bins"                          # Check only the specified binary(s)
        --examples                                                             # Check all examples
        --example: string@"nu-complete cargo target-examples"                  # Check only the specified example
        --tests                                                                # Check all tests
        --test: string@"nu-complete cargo target-tests"                        # Check only the specified test target
        --benches                                                              # Check all benches
        --bench: string@"nu-complete cargo target-benches"                     # Check only the specified bench target
        --all-targets                                                          # Check all targets
        --doc                                                                  # Test only this library's documentation
        --features(-F): string                                                 # Space or comma separated list of features to activate
        --all-features                                                         # Activate all available features
        --no-default-features                                                  # Do not activate the `default` feature
        --jobs(-j): int                                                        # Number of parallel jobs, defaults to # of CPUs
        --release(-r)                                                          # Check artifacts in release mode, with optimizations
        --profile: string@"nu-complete cargo-profiles"                         # Check artifacts with the specified profile
        --target: string@"nu-complete cargo-targets"                           # Check for the target triple
        --target-dir: directory                                                # Directory for all generated artifacts
        --unit-graph                                                           # Output build graph in JSON (unstable)
        --timings                                                              # Emit build timings
        --manifest-path: path                                                  # Path to Cargo.toml
        --lockfile-path: path                                                  # Path to Cargo.lock (unstable)
        --ignore-rust-version                                                  # Ignore `rust-version` specification in packages
        --locked                                                               # Assert that `Cargo.lock` will remain unchanged
        --offline                                                              # Run without accessing the network
        --frozen                                                               # Equivalent to specifying both --locked and --offline
    ]
}

alias "cargo c" = cargo check
alias "cargo b" = cargo build
alias "cargo r" = cargo run
alias "cargo t" = cargo test

use cargo-completions *
