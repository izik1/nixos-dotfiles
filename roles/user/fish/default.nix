{ pkgs, config, lib, ... }:
{
  home.packages = with pkgs; [
    fishPlugins.fzf-fish
    # I probably want this in like `rustation` as well, but fzf-fish depends on it
    fd
  ];

  programs.fzf = {
    enable = true;
    enableFishIntegration = false;
  };

  programs.starship = {
    enable = true;
    enableFishIntegration = true;
  };

  programs.fish = {
    enable = true;
  };
}
