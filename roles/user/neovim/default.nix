{ pkgs, config, lib, ... }:
{
  # todo: dry?
  home.file = {
    ".config/nvim/misc.vim".source = ./misc.vim;
    ".config/nvim/ctrlp.vim".source = ./ctrlp.vim;
    ".config/nvim/coc.vim".source = ./coc.vim;
  };

  programs.neovim = {
    enable = true;
    vimAlias = true;
    withNodeJs = true;
    coc = {
      enable = true;
      settings = {
        "coc.preferences.extensionUpdateCheck" = "daily";
      };
    };

    plugins = with pkgs.vimPlugins; [
      ctrlp-vim
      coc-rust-analyzer
      coc-tabnine
      vim-nix
      glow-nvim
    ];

    extraConfig = ''
      let g:nvim_config_root = stdpath('config')
      let g:extra_files = ['misc.vim', 'ctrlp.vim', 'coc.vim']
      let g:extra_files = ['misc.vim', 'ctrlp.vim']

      lua << EOF
      require('glow').setup({style = "dark"})
      EOF

      for file in g:extra_files
          execute 'source ' . g:nvim_config_root . '/' . file
      endfor
    '';
  };
}

