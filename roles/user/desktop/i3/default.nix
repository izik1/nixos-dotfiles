{ pkgs, config, lib, ... }:
{
  imports = [
    ./i3status-rust.nix
    ../shared/alacritty
    ../shared/rofi.nix
    ../shared/fonts.nix
  ];

  # fixme: extract
  # picom appears to be very fucky.
  # services.picom.enable = false;
  services.dunst.enable = true;

  home.packages = [
    pkgs.nitrogen
  ];

  xsession = {
    enable = true;
    windowManager.i3 = {
      enable = true;

      # use rec to wipe out default settings (see: https://github.com/wiltaylor/dotfiles/blob/982280e5a019f87f0b944b0edc0d7c49fa6b9242/roles/users/desktop/i3wm/default.nix#L84)

      config =
        let
          resizeMode = "resize";
          chordMode = "manage [i]3 (v), [m]edia";
          i3ManagementMode = "i3: reload [c]onfig, [r]estart [l]ock";
          mediaMode = "media: vim keys (<> skip, ^v volume), [p]lay/[p]ause, [m]ute";
          # todo: *more* let in
        in
        rec {
          modifier = "Mod4";

          startup = [
            {
              command = "xrandr --output DisplayPort-0 --primary";
              always = false;
              notification = false;
            }
            {
              command = "${pkgs.nitrogen}/bin/nitrogen --restore";
              always = true;
              notification = false;
            }
          ];

          bars = [
            {
              position = "top";
              statusCommand = "${pkgs.i3status-rust}/bin/i3status-rs ~/.config/i3status-rust/config-top.toml";
              fonts = {
                names = [ "DejaVu Sans Mono" "Font Awesome 6 Free" ];
                size = 9.0;
              };

              colors = {
                separator = "#666666";
                background = "#222222";
                statusline = "#dddddd";
                focusedWorkspace = { background = "#0088CC"; border = "#0088CC"; text = "#ffffff"; };
                activeWorkspace = { background = "#333333"; border = "#333333"; text = "#ffffff"; };
                inactiveWorkspace = { background = "#333333"; border = "#333333"; text = "#888888"; };
                urgentWorkspace = { background = "#2f343a"; border = "#900000"; text = "#ffffff"; };
              };

              trayOutput = "none";

              extraConfig = ''
                output primary
              '';
            }
            {
              position = "top";
              fonts = {
                names = [ "Victor Mono" "Font Awesome 6 Free" ];
                size = 12.0;
              };

              colors = {
                separator = "#666666";
                background = "#222222";
                statusline = "#dddddd";
                focusedWorkspace = { background = "#0088CC"; border = "#0088CC"; text = "#ffffff"; };
                activeWorkspace = { background = "#333333"; border = "#333333"; text = "#ffffff"; };
                inactiveWorkspace = { background = "#333333"; border = "#333333"; text = "#888888"; };
                urgentWorkspace = { background = "#2f343a"; border = "#900000"; text = "#ffffff"; };
              };

              trayOutput = "DisplayPort-1";

              extraConfig = ''
                output nonprimary
              '';
            }
          ];

          gaps = {
            inner = 8;
          };

          terminal = "alacritty";

          defaultWorkspace = "workspace number 1";

          modes = {
            "${resizeMode}" = {
              h = "resize shrink width 10 px or 10 ppt";
              j = "resize grow height 10 px or 10 ppt";
              k = "resize shrink height 10 px or 10 ppt";
              l = "resize grow width 10 px or 10 ppt";

              # same bindings, but for the arrow keys
              Left = "resize shrink width 10 px or 10 ppt";
              Down = "resize grow height 10 px or 10 ppt";
              Up = "resize shrink height 10 px or 10 ppt";
              Right = "resize grow width 10 px or 10 ppt";

              # back to normal: Enter or Escape or $mod+r
              Return = "mode default";
              Escape = "mode default";
              "${modifier}+r" = "mode default";
            };

            "${chordMode}" = {
              Escape = "mode default";
              Return = "mode default";
              b = "mode default";
              v = "mode \"${i3ManagementMode}\"";
              i = "mode \"${i3ManagementMode}\"";
              ## d = "mode $mode_launch";
              m = "mode \"${mediaMode}\"";
            };

            "${i3ManagementMode}" = {
              Escape = "mode default";
              c = "mode default, reload";
              r = "mode default, restart";
              l = "mode default, exec xset dpms force off, exec i3lock";
            };

            "${mediaMode}" = {
              # media management
              # escape builds bad habits on this keyboard
              #    bindsym Escape mode "default"
              Return = "mode default";
              # todo: hide this behind player specific?
              h = "exec ${pkgs.playerctl}/bin/playerctl --player=spotify previous";
              j = "exec ${pkgs.pamixer}/bin/pamixer --decrease -5%";
              k = "exec ${pkgs.pamixer}/bin/pamixer --increase +5%";
              l = "exec ${pkgs.playerctl}/bin/playerctl --player=spotify next";
              p = "exec ${pkgs.playerctl}/bin/playerctl --player=spotify play-pause";
              m = "exec ${pkgs.pamixer}/bin/pamixer --toggle-mute";
            };
          };

          keybindings = {
            "${modifier}+Return" = "exec ${pkgs.alacritty}/bin/alacritty";
            "${modifier}+Shift+q" = "kill";
            "${modifier}+d" = "exec rofi -show combi";
            "${modifier}+h" = "focus left";
            "${modifier}+j" = "focus down";
            "${modifier}+k" = "focus up";
            "${modifier}+l" = "focus right";
            "${modifier}+Shift+h" = "move left";
            "${modifier}+Shift+j" = "move down";
            "${modifier}+Shift+k" = "move up";
            "${modifier}+Shift+l" = "move right";
            "${modifier}+Shift+v" = "split h";
            "${modifier}+v" = "split v";
            "${modifier}+f" = "fullscreen toggle";
            "${modifier}+s" = "layout stacking";
            "${modifier}+w" = "layout tabbed";
            "${modifier}+e" = "layout toggle split";
            "${modifier}+Shift+space" = "floating toggle";
            "${modifier}+space" = "focus mode_toggle";
            "${modifier}+a" = "focus parent";
            "${modifier}+1" = "workspace number 1";
            "${modifier}+2" = "workspace number 2";
            "${modifier}+3" = "workspace number 3";
            "${modifier}+4" = "workspace number 4";
            "${modifier}+5" = "workspace number 5";
            "${modifier}+6" = "workspace number 6";
            "${modifier}+7" = "workspace number 7:Spotify";
            "${modifier}+8" = "workspace number 8";
            "${modifier}+9" = "workspace number 9";
            "${modifier}+0" = "workspace number 10";
            "${modifier}+Shift+1" = "move container to workspace number 1";
            "${modifier}+Shift+2" = "move container to workspace number 2";
            "${modifier}+Shift+3" = "move container to workspace number 3";
            "${modifier}+Shift+4" = "move container to workspace number 4";
            "${modifier}+Shift+5" = "move container to workspace number 5";
            "${modifier}+Shift+6" = "move container to workspace number 6";
            "${modifier}+Shift+7" = "move container to workspace number 7:Spotify";
            "${modifier}+Shift+8" = "move container to workspace number 8";
            "${modifier}+Shift+9" = "move container to workspace number 9";
            "${modifier}+Shift+0" = "move container to workspace number 10";
            "${modifier}+Shift+c" = "reload";
            "${modifier}+Shift+r" = "restart";
            "${modifier}+Shift+e" = "exec \"i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'\"";
            "${modifier}+Shift+greater" = "move container to output left";
            "${modifier}+Shift+less" = "move container to output right";
            "${modifier}+r" = "mode ${resizeMode}";
            "${modifier}+b" = "mode \"${chordMode}\"";
          };

          window.commands = [
            # part of gaps
            {
              command = "border pixel 0";
              criteria = { class = ".*"; };
            }
          ];

        };
    };
  };
}
