{ pkgs, config, lib, ... }:
{
  programs.i3status-rust = {
    enable = true;
    bars = {
      top = {
        theme = "modern";
        icons = "awesome6";
        blocks = [
          {
            block = "time";
            format = " $icon $timestamp.datetime(f:'%F %T') ";
            timezone = "US/Pacific";
            interval = 1;
          }
          {
            block = "cpu";
            interval = 1;
            format = " $icon $barchart $utilization $frequency ";
          }
          {
            block = "memory";
            format = " $icon $mem_used/$mem_total($mem_used_percents) ";
            interval = 5;
            warning_mem = 80;
            critical_mem = 95;
          }
          { block = "sound"; }
          {
            block = "sound";
            device_kind = "source";
          }
          {
            block = "music";
            player = "spotify";
            format = " $icon  {$prev $play $next $combo.str(max_w:25,rot_interval:0.5) |}";
            # on_collapsed_click = "${pkgs.spotifywm}/bin/spotifywm";
            # click 
            # on_click = "i3-msg \"[class=Spotify] focus\"";
          }
          { block = "uptime"; }
        ];
      };
    };
  };
}
