{ pkgs, config, lib, ... }:
{
  imports = [
    ../i3/i3status-rust.nix
    ../shared/alacritty
    ../shared/rofi.nix
    ../shared/fonts.nix
    ../shared/kitty
  ];

  programs.rofi.package = pkgs.rofi-wayland;
  programs.rofi.plugins = [ pkgs.my.rofi-calc-wayland ];

  services.dunst.enable = true;
  services.dunst.settings = {
    global = {
      monitor = "DP-1";
    };
  };


  home.packages = [
    pkgs.swaybg
    pkgs.slurp
    pkgs.grim
    pkgs.wl-clipboard
  ];

  xdg.portal = {
    enable = false;
    extraPortals = [
      pkgs.xdg-desktop-portal-wlr
      pkgs.xdg-desktop-portal-gtk
    ];

    config.common = {
      default = ["wlr" "gtk"];

    };
  };

  wayland = {
    windowManager.sway = {
      enable = true;

      # use rec to wipe out default settings (see: https://github.com/wiltaylor/dotfiles/blob/982280e5a019f87f0b944b0edc0d7c49fa6b9242/roles/users/desktop/i3wm/default.nix#L84)

      config =
        let
          workspaces = {
            # escape code: fa-message.
            im = "2: &#xf27a;";
            # escape code: fa-spotify.
            spotify = "7: &#xf1bc;";
            # escape code: fa-steam.
            steam = "10: &#xf1b6;";
          };
          resizeMode = "resize";
          chordMode = "manage [i]3 (v), [m]edia, [o]utput";
          i3ManagementMode = "i3: reload [c]onfig, [r]estart [l]ock";
          mediaMode = "media: vim keys (<> skip, ^v volume), [p]lay/[p]ause, [m]ute";
          outputMode = "toggle output: DP-[1], DP-[2]";

          # todo: Write a nushell script for these, name it... grimoire?
          grimSnap = pkgs.resholve.writeScriptBin "grimSnap" {
            inputs = with pkgs; [ grim slurp wl-clipboard ];
            interpreter = "${pkgs.dash}/bin/dash";
          } ''
              grim -g "$(slurp)" - | wl-copy
          '';
          grimOutputSnap = pkgs.resholve.writeScriptBin "grimOutputSnap" {
            inputs = with pkgs; [ grim slurp wl-clipboard ];
            interpreter = "${pkgs.dash}/bin/dash";
          } ''
              grim -o "$(slurp -o -f '%o')" - | wl-copy
          '';

          # todo: *more* let in
        in
        rec {
          modifier = "Mod4";

          startup = [
            {
              command = "${pkgs.waypaper}/bin/waypaper --restore";
            }
          ];

          bars = [
            {
              position = "top";
              statusCommand = "${pkgs.i3status-rust}/bin/i3status-rs ~/.config/i3status-rust/config-top.toml";
              fonts = {
                names = [ "Anonymous Pro" "Font Awesome 6 Free" ];
                size = 15.0;
              };

              colors = {
                separator = "#666666";
                background = "#222222";
                statusline = "#dddddd";
                focusedWorkspace = { background = "#0088CC"; border = "#0088CC"; text = "#ffffff"; };
                activeWorkspace = { background = "#333333"; border = "#333333"; text = "#ffffff"; };
                inactiveWorkspace = { background = "#333333"; border = "#333333"; text = "#888888"; };
                urgentWorkspace = { background = "#2f343a"; border = "#900000"; text = "#ffffff"; };
              };

              trayOutput = "none";

              extraConfig = ''
                output DP-1
                pango_markup enabled
              '';
            }
            {
              position = "top";
              fonts = {
                names = [ "Anonymous Pro" "Font Awesome 6 Free" ];
                size = 15.0;
              };

              colors = {
                separator = "#666666";
                background = "#222222";
                statusline = "#dddddd";
                focusedWorkspace = { background = "#0088CC"; border = "#0088CC"; text = "#ffffff"; };
                activeWorkspace = { background = "#333333"; border = "#333333"; text = "#ffffff"; };
                inactiveWorkspace = { background = "#333333"; border = "#333333"; text = "#888888"; };
                urgentWorkspace = { background = "#2f343a"; border = "#900000"; text = "#ffffff"; };
              };

              trayOutput = "DP-2";

              extraConfig = ''
                output DP-2
                pango_markup enabled
              '';
            }
          ];

          fonts = {
            names = [ "Victor Mono" "Font Awesome 6 Free" ];
            size = 10.0;
          };

          gaps = {
            inner = 8;
          };

          terminal = "alacritty";

          defaultWorkspace = "workspace number 1";

          workspaceOutputAssign = [
            {
              workspace = workspaces.im;
              output = "DP-2";
            }
            {
              workspace = "5";
              output = "DP-2";
            }
            {
              workspace = workspaces.spotify;
              output = "DP-2";
            }
            {
              workspace = workspaces.steam;
              output = "DP-1";
            }
          ];

          assigns = {
            "${workspaces.steam}" = [{
              class = "^steam$";
            }];
            "${workspaces.im}" = [
              {
                app_id = "^discord$";
              }
              {
                app_id = "^Slack$";
              }
            ];
          };

          modes = {
            "${resizeMode}" = {
              h = "resize shrink width 10 px or 10 ppt";
              j = "resize grow height 10 px or 10 ppt";
              k = "resize shrink height 10 px or 10 ppt";
              l = "resize grow width 10 px or 10 ppt";

              # same bindings, but for the arrow keys
              Left = "resize shrink width 10 px or 10 ppt";
              Down = "resize grow height 10 px or 10 ppt";
              Up = "resize shrink height 10 px or 10 ppt";
              Right = "resize grow width 10 px or 10 ppt";

              # back to normal: Enter or Escape or $mod+r
              Return = "mode default";
              Escape = "mode default";
              "${modifier}+r" = "mode default";
            };

            "${chordMode}" = {
              Escape = "mode default";
              Return = "mode default";
              b = "mode default";
              v = "mode \"${i3ManagementMode}\"";
              i = "mode \"${i3ManagementMode}\"";
              ## d = "mode $mode_launch";
              m = "mode \"${mediaMode}\"";
              o = "mode \"${outputMode}\"";
            };

            "${i3ManagementMode}" = {
              Escape = "mode default";
              c = "mode default, reload";
              r = "mode default, restart";
              l = "mode default, exec xset dpms force off, exec i3lock";
            };

            "${mediaMode}" = {
              # media management
              # escape builds bad habits on this keyboard
              #    bindsym Escape mode "default"
              Return = "mode default";
              # todo: hide this behind player specific?
              h = "exec ${pkgs.playerctl}/bin/playerctl --player=spotify previous";
              j = "exec ${pkgs.pamixer}/bin/pamixer --decrease -5%";
              k = "exec ${pkgs.pamixer}/bin/pamixer --increase +5%";
              l = "exec ${pkgs.playerctl}/bin/playerctl --player=spotify next";
              p = "exec ${pkgs.playerctl}/bin/playerctl --player=spotify play-pause";
              m = "exec ${pkgs.pamixer}/bin/pamixer --toggle-mute";
            };

            "${outputMode}" = {
              Return = "mode default";
              "1" = "output DP-1 toggle";
              "2" = "output DP-2 toggle";
            };
          };

          keybindings = {
            "${modifier}+Return" = "exec ${pkgs.alacritty}/bin/alacritty";
            "${modifier}+Shift+q" = "kill";
            "${modifier}+d" = "exec rofi -show combi";
            "${modifier}+h" = "focus left";
            "${modifier}+j" = "focus down";
            "${modifier}+k" = "focus up";
            "${modifier}+l" = "focus right";
            "${modifier}+Shift+h" = "move left";
            "${modifier}+Shift+j" = "move down";
            "${modifier}+Shift+k" = "move up";
            "${modifier}+Shift+l" = "move right";
            "${modifier}+Shift+v" = "split h";
            "${modifier}+v" = "split v";
            "${modifier}+f" = "fullscreen toggle";
            "${modifier}+s" = "layout stacking";
            "${modifier}+w" = "layout tabbed";
            "${modifier}+e" = "layout toggle split";
            "${modifier}+Shift+space" = "floating toggle";
            "${modifier}+space" = "focus mode_toggle";
            "${modifier}+a" = "focus parent";
            "${modifier}+1" = "workspace number 1";
            "${modifier}+2" = "workspace number \"${workspaces.im}\"";
            "${modifier}+3" = "workspace number 3";
            "${modifier}+4" = "workspace number 4";
            "${modifier}+5" = "workspace number 5";
            "${modifier}+6" = "workspace number 6";
            "${modifier}+7" = "workspace number \"${workspaces.spotify}\"";
            "${modifier}+8" = "workspace number 8";
            "${modifier}+9" = "workspace number 9";
            "${modifier}+0" = "workspace number \"${workspaces.steam}\"";
            "${modifier}+Shift+1" = "move container to workspace number 1";
            "${modifier}+Shift+2" = "move container to workspace number \"${workspaces.im}\"";
            "${modifier}+Shift+3" = "move container to workspace number 3";
            "${modifier}+Shift+4" = "move container to workspace number 4";
            "${modifier}+Shift+5" = "move container to workspace number 5";
            "${modifier}+Shift+6" = "move container to workspace number 6";
            "${modifier}+Shift+7" = "move container to workspace number \"${workspaces.spotify}\"";
            "${modifier}+Shift+8" = "move container to workspace number 8";
            "${modifier}+Shift+9" = "move container to workspace number 9";
            "${modifier}+Shift+0" = "move container to workspace number \"${workspaces.steam}\"";
            "${modifier}+Shift+c" = "reload";
            "${modifier}+Shift+r" = "restart";
            "${modifier}+Shift+e" = "exec \"swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your wayland session.' -B 'Yes, exit sway' 'swaymsg exit'\"";
            "${modifier}+Shift+greater" = "move container to output left";
            "${modifier}+Shift+less" = "move container to output right";
            "${modifier}+r" = "mode ${resizeMode}";
            "${modifier}+b" = "mode \"${chordMode}\"";
            "${modifier}+p" = "exec \"${grimSnap}\"/bin/grimSnap";
            "${modifier}+Shift+p" = "exec \"${grimOutputSnap}\"/bin/grimOutputSnap";
          };

          window.commands = [
            # part of gaps
            {
              command = "border pixel 0";
              criteria = { class = ".*"; };
            }
          ];
        };
    };
  };
}

