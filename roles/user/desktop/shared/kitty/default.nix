{ pkgs, config, lib, ... }:
{
  programs.kitty = {
    enable = true;
    themeFile = "citylights";
    font = {
      name = "Victor Mono";
      size = 24;
    };
  };
}
