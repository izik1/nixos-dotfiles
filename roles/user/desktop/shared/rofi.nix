{ pkgs, config, lib, ... }:
{
  programs.rofi =
    {
      enable = true;
      theme = "Arc-Dark";
      plugins = lib.mkDefault [ pkgs.rofi-calc ];
      extraConfig = {
        modi = "combi,calc,ssh";
        combi-modes = "window,drun";

        # modi = "window,drun,ssh";
        show-icons = true;
      };
    };
}
