{ pkgs, ... }:
with pkgs;
{
  backup = (callPackage ./backup.nix { });
  unityhub = (callPackage ./unityhub { });
  synergism = (callPackage ./synergism { });
  djot = (callPackage ./djot { });
  watershot = (callPackage ./watershot { });
  anki-beta = (callPackage ./anki-beta { });
  anki-beta-bin = (callPackage ./anki-beta/bin.nix { });
  iamb-desktop = (callPackage ./iamb-desktop { });
  # big hack:
  rofi-calc-wayland = pkgs.rofi-calc.overrideAttrs (old: {
    pname = "rofi-calc-wayland";
    buildInputs = [
      rofi-wayland-unwrapped
      libqalculate
      glib
      cairo
    ];
  });
}
