{ stdenv, buildNpmPackage, fetchFromGitHub, lib }:

# wip. (broken)
buildNpmPackage rec {
    pname = "djot";
    version = "0.2.0";

    src = fetchFromGitHub {
    owner = "jgm";
    repo = "djot.js";
    rev = version;
    sha256 = "sha256-Cc1pDlgggXihARZSdAhPKANRhByou9/sWnifbsWwMr4=";
    };
}
