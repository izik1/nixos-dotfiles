#!@bash@

log() {
  echo "[$(date +%FT%T%z)][$1]: ${*:2}" >&2
}

err() {
  log "ERROR" "$@"
}

warn() {
  log "WARN" "$@"
}
  
info() {
  log "INFO" "$@"
}

readonly -f log err warn info

exit_trap () {
  local lc="$BASH_COMMAND" rc=$?
  err "Command [$lc] exited with code [$rc]"
}

readonly -f exit_trap

readonly SNAPSHOT_DIR="/snapshots/@home"
readonly SNAPSHOT_OUTPUT_DIR="/media/backup/snapshots-home"
readonly LATEST_SNAPSHOT="$SNAPSHOT_DIR/latest"

check_uid() {
  if [ $UID -ne 0 ];then
    err "Must be root"
    exit 1
  fi
}

readonly -f check_uid

snap() {
  local now
  now="$(date -u +%FT%TZ)"

  local snapshot_name="home-$now"
  local snapshot="$SNAPSHOT_DIR/$snapshot_name"

  info "starting snapshot: \`$snapshot_name\`"

  if ! @btrfs@ subvolume snapshot -r /home "$snapshot" >&2; then
    err "snapshot command failed"
    return 1
  fi

  info "created snapshot: \`$snapshot\`"
  echo "$snapshot"
}

readonly -f snap

main() {
  check_uid

  trap exit_trap EXIT
  # `set -euo pipefile` but more readable
  set -o nounset -o pipefail -o errexit
  
  local snapshot
  if ! snapshot="$(snap)"; then
    err "failed to create snapshot"
    exit 1
  fi

  local prev_full
  prev_full="$(realpath "$LATEST_SNAPSHOT")"
  local prev_name
  prev_name="$(basename "$prev_full")"

  local flags=(--verbose send --proto 0 --compressed-data)
    
  if [ "$prev_name" = "latest" ]; then
    info "no previous snapshot found, performing non-incremental backup"
  elif [ ! -d "$SNAPSHOT_OUTPUT_DIR/$prev_name" ]; then
    err "previous snapshot found ($prev_name), but it wasn't backed up properly; exiting for manual resolution"
    # avoid spurious message
    trap EXIT
    exit 1
  else 
    info "found previous snapshot [$prev_full]"
    flags+=(-p "$prev_full")
  fi

  flags+=("$snapshot")

  info "sending snapshot [$snapshot] -> [$SNAPSHOT_OUTPUT_DIR/$(basename "$snapshot")"

  # the force decompress is because we assume that the receiver may have higher compression burden than the sender.
  if ! @btrfs@ "${flags[@]}" | @btrfs@ --verbose receive "$SNAPSHOT_OUTPUT_DIR/" >&2; then
    err "failed to send snapshot [$snapshot] -> [$SNAPSHOT_OUTPUT_DIR]"
    # avoid spurious message
    trap EXIT
    exit 1
  fi

  info "backed up [$snapshot] -> [$SNAPSHOT_OUTPUT_DIR/$(basename "$snapshot")]"

  info "tagging [$snapshot] as latest"

  if ! ln --symbolic --no-dereference --force "$snapshot" "$LATEST_SNAPSHOT"; then
    err "failed to tag snapshot"
    # avoid spurious message
    trap EXIT
    exit 1
  fi

  # avoid spurious message
  trap EXIT
}

readonly -f main

main "$@"
