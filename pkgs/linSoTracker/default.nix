{ python3Packages, fetchFromGitHub }:

let 
  pname = "linSoTracker";
  version = "2.2";
in
  python3Packages.buildPythonApplication {
    pname = pname;
    version = version;
    src = fetchFromGitHub {
      owner = "linsorak";
      repo = "LinSoTracker";
      rev = "v${version}";
      sha256 = "sha256-9Y+lmQFOwljSHsEMqhSoGyAjbebwKFHIj3fuqhZv3FY=";
    };
  }
