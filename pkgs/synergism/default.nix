{ lib, rustPlatform, fetchFromGitHub, pkg-config, gtk3, webkitgtk, wrapGAppsHook }:
let
  pname = "synergism";
  version = "c47c9ec13107b8096b310fe99023c9273d65e503";
  src = fetchFromGitHub {
    owner = "Pseudo-Corp";
    repo = "steam";
    rev = version;
    sha256 = "sha256-VgdI9M+eGFp8jBO508fuChadru7l5N7eBdDz0Yed/Hw=";
  };
in
rustPlatform.buildRustPackage rec {
  inherit version src pname;
  
  sourceRoot = "source/src-tauri";
  cargoSha256 = "sha256-J48ZmMWIrpzMEGbLqkv1JHJ+U1KOt9TwRcyQg9lNqEs=";

  nativeBuildInputs = [
    pkg-config
  ];

  buildInputs = [
    gtk3
    webkitgtk
    wrapGAppsHook
  ];

  postInstall = ''
    mv $out/bin/steam $out/bin/synergism
  '';
}
