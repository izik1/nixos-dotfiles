{ fetchurl
, stdenv
, autoPatchelfHook
, cups
, dpkg
, gtk3
, alsa-lib
, libdrm
, mesa
, libkrb5
, lttng-ust
, cpio
, libsecret
, lib
, makeWrapper
, mono
, expat
, at-spi2-atk
, at-spi2-core
, atk
, cairo
, curl
, dbus
, fontconfig
, freetype
, gdk-pixbuf
, glib
, gnome2
, libGL
, libappindicator-gtk3
, libnotify
, libuuid
, libxcb
, libxkbcommon
, nspr
, nss
, pango
, systemd
, xorg
, icu
, openssl
}:
stdenv.mkDerivation rec {
  pname = "unityhub";
  version = "3.2.0";
  src = fetchurl
    {
      url = "https://hub.unity3d.com/linux/repos/deb/pool/main/u/unity/unityhub_amd64/unityhub-amd64-3.2.0.deb";
      sha256 = "663de26c61eae85ef996ea02e9e20b7fbc9757e9f2d42db66ce81b2816d1e608";
    };
  nativeBuildInputs = [ autoPatchelfHook makeWrapper ];
  buildInputs = [
    stdenv.cc.cc.lib
    cups
    gtk3
    alsa-lib
    nss
    libdrm
    mesa
    xorg.libxshmfence
    libkrb5
    lttng-ust
    expat
  ];

  libPath = lib.makeLibraryPath [
    libsecret
    stdenv.cc.cc.lib
    cups
    alsa-lib
    libkrb5
    lttng-ust
    alsa-lib
    at-spi2-atk
    at-spi2-core
    atk
    cairo
    cups
    curl
    dbus
    expat
    fontconfig
    freetype
    gdk-pixbuf
    glib
    gnome2.GConf
    gtk3
    libGL
    libappindicator-gtk3
    libdrm
    libnotify
    libuuid
    libxcb
    libxkbcommon
    mesa
    nspr
    nss
    pango
    stdenv.cc.cc
    systemd
    xorg.libX11
    xorg.libXScrnSaver
    xorg.libXcomposite
    xorg.libXcursor
    xorg.libXdamage
    xorg.libXext
    xorg.libXfixes
    xorg.libXi
    xorg.libXrandr
    xorg.libXrender
    xorg.libXtst
    xorg.libxkbfile
    xorg.libxshmfence
    icu
    openssl
  ] + ":${stdenv.cc.cc.lib}/lib64";

  unpackPhase = ''
    mkdir -p $TMP/${pname} $out/{opt/${pname},bin}
    cp $src $TMP/${pname}.deb
    ar vx ${pname}.deb
    ls
    tar --no-overwrite-dir -xvf data.tar.bz2 -C $TMP/${pname}/

  '';

  installPhase = ''
    # rm -r $TMP/${pname}/opt/${pname}/resources/app.asar.unpacked/lib/linux/7z
    cp -R $TMP/${pname}/opt/${pname}/* $out/opt/${pname}
    ln -sf $out/opt/${pname}/${pname} $out/bin/${pname}
  '';


  preFixup = ''
    patchelf --replace-needed liblttng-ust.so.0 liblttng-ust.so $out/opt/${pname}/UnityLicensingClient_V1/libcoreclrtraceptprovider.so
  '';

  postFixup = ''
    makeWrapper $out/opt/${pname}/${pname}-bin $out/bin/${pname} \
      --prefix LD_LIBRARY_PATH : "${libPath}" \
      "''${gappsWrapperArgs[@]}"
  '';
}

