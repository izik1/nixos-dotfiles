{
  lib,
  stdenv,
  substituteAll,
  makeDesktopItem,
  writeScriptBin,
  symlinkJoin,
  dash,
  iamb,
  kitty
}:
let
  srcFile = substituteAll {
    src = ./iamb-desktop.sh;
    dash = "${dash}/bin/dash";
    iamb = "${iamb}/bin/iamb";
    kitty = "${kitty}/bin/kitty";
  };
  src = builtins.readFile srcFile;
  iamb-desktop-sh = writeScriptBin "iamb-desktop" src;  

  desktopItem = makeDesktopItem {
    name = "iamb-desktop";
    exec = "iamb-desktop";
    comment = "Terminal-based client for Matrix for the Vim addict.";
    desktopName = "iamb";
    type = "Application";
    categories = [ "Audio" "AudioVideo" ];
    terminal = false;
    startupWMClass = "iamb-desktop";
  };
in
symlinkJoin {
  name = "iamb-desktop";
  paths = [
    iamb-desktop-sh
    desktopItem
  ];
}
