{ pkgs, config, lib, ... }:
{
  # prepare for the flake
  nix = {
    extraOptions = ''
      experimental-features = nix-command flakes
    '';

    settings.auto-optimise-store = true;
  };

  # Pretty reasonable to trust `cache.lix.systems` when I'm trusting their builder.
  nix.settings.extra-substituters = [
    "https://cache.lix.systems"
  ];

  nix.settings.trusted-public-keys = [
    "cache.lix.systems:aBnZUw8zA7H35Cz2RyKFVs3H4PlGTLawyY5KRbvJR8o="
  ];
}
