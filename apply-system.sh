#!/usr/bin/env bash

# extremely pared down version of `nixos-rebuild.sh` that only `doas`' at the right time.

set -e
set -o pipefail
shopt -s inherit_errexit

flake="$(dirname "$(realpath "$0")")#"

# For convenience, use the hostname as the default configuration to
# build from the flake.
if [[ -n $flake ]]; then
    if [[ $flake =~ ^(.*)\#([^\#\"]*)$ ]]; then
       flake="${BASH_REMATCH[1]}"
       flakeAttr="${BASH_REMATCH[2]}"
    fi
    if [[ -z $flakeAttr ]]; then
        hostname="$(cat /proc/sys/kernel/hostname)"
        if [[ -z $hostname ]]; then
            hostname=default
        fi
        flakeAttr="nixosConfigurations.\"$hostname\""
    else
        flakeAttr="nixosConfigurations.\"$flakeAttr\""
    fi
fi

# nixos-rebuild switch --flake "${flake_path}#"
tmpDir="$(mktemp -t -d nixos-rebuild.XXXXXX)"

nix build "$flake#$flakeAttr.config.system.build.toplevel" --out-link "${tmpDir}/result"

pathToConfig="$(readlink -f "${tmpDir}/result")"

echo "finished building: \`$pathToConfig\`"

doas bash ./switch-system.sh "$pathToConfig"
